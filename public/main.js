var socket = io();
var socketChat = 'chat';
var socketAdmin = 'admin';

// Login - happens on load
$.get('/user').success(function (res) {
      setId(res.id);
      $('#messages').append($("<li id='admin'>").text("===== Welcome to the GlobeKeeper Chat Service! ====="));
      // send join message
      socket.emit(socketAdmin, {'id' : getId(), 'join' : true});
        }).error(function () {
          console.log('error');
          });

// Receive messages
socket.on(socketChat, function(msg){
  var sender = `user${msg.id}`;

    // Replace username with 'me'
    if(isMe(msg.id)) sender = 'me';
    var formalMessage = `[${msg.date}] ${sender} : ${msg.msg}`;
   $('#messages').append($("<li>").text(formalMessage));
 });

// Before leaving, send 'leave' message
 $(window).on('beforeunload', function(){
    socket.emit(socketAdmin, {'id' : getId(), 'join' : false});
});

 // Receive admin messages (join/leave chat)
 socket.on(socketAdmin, function(msg){
   var formalMessage = msg.msg;
    $('#messages').append($("<li id='admin'>").text(formalMessage));
  });

// Send messagse
function sendMessage() {
  var value = $('#message').val();
  if(value) {
    socket.emit(socketChat, {'id' : getId(), 'msg' : value});
    $('#message').val('');
    return false;
  }
}

function isMe(id) {
  return getId() === id;
}

function setId(id) {
  sessionStorage.setItem('id', id);
}

function getId() {
  return sessionStorage.id;
}
