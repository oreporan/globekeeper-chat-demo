 #!/bin/bash
#  set -e
PORT1="3001"
PORT2="3002"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


 function clean {
   echo "\nKilling all previous node instances..."
   killall -9 node 2> /dev/null
   ./rabbitMQ/rabbitmq_server-3.6.1/sbin/rabbitmqctl stop 2> /dev/null
   echo "Bye!"
}

function runNode {
  osascript -e 'tell app "Terminal"
    do script "cd '$DIR' && node app.js '$1'"
    end tell'
}

trap clean INT

echo "========Welcome to the GlobeKeeper chat service=========="

echo "Unpacking rabbitMQ..."
tar xf rabbitMQ/rabbitmq-server-mac-standalone-3.6.1.tar.xz -C rabbitMQ
echo "Finished unpacking rabbitMQ"

echo "Starting local rabbitMQ server"
./rabbitMQ/rabbitmq_server-3.6.1/sbin/rabbitmq-server -detached 2> /dev/null
echo "rabbitMQ server running..."

echo "Installing NPM packages"
npm install
echo "Starting 2 node servers on port '$PORT1' and '$PORT2'..."
killall -9 node 2> /dev/null
runNode $PORT1
runNode $PORT2
sleep 2
open "http://localhost:3001"
