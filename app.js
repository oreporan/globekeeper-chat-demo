var express = require('express');
//var bodyParser = require('body-parser');
var http = require('http');
var amqp = require('amqp');

// Setup socket.io
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// User public directory
app.use('/public', express.static(__dirname + '/public'));

// Connect to local rabbitMQ
var path = 'amqp://localhost';
var rabbitConn = amqp.createConnection({'url' : path});

var chatExchange;
rabbitConn.on('ready', function () {
    console.log('rabbitMQ ready to work');
    chatExchange = rabbitConn.exchange('chat', {'type': 'fanout'});
});

// Connect to socketIO
io.on('connection', function(socket){

/*
* Publisher code - get messages from admin and chat, and publish them
*/

// Admin socket, for admin messages
  socket.on('admin', function(msg){
    var formalMessage = msg.join ? `user${msg.id} has joined the chat` : `user${msg.id} has left the chat`;
    chatExchange.publish('', buildMessageObject('admin', formalMessage));
  });

  // Chat socket, for chat messages
  socket.on('chat', function(msg){
    chatExchange.publish('', buildMessageObject(msg.id, msg.msg));
  });

  /*
  * Subsrciber code - get messages from rabbitConnection and emit them to relevant sockets
  */

  // Connect to queue
  rabbitConn.queue('', {exclusive: false}, function (q) {
      //Bind to chat
      q.bind('chat', '');

      //Subscribe When a message comes, send to client
      q.subscribe(function (message) {
        var socketToUse = message.id === 'admin' ? 'admin' : 'chat';
          socket.emit(socketToUse, message);
      });
  });
});

var port = process.argv[2] || 3000;
server.listen(port);
console.log(`Nodejs listening on port ${port} \nconnecting to rabbitMQ server..`);

/**
* joins the chat room
* @return the generated clientID
**/
app.get('/user', function(req,res) {
  var clientId = generateRandomId();
  res.send({'id' : clientId});
});

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

/**
* Helper methods
**/

// Builds the messages for rabbitMQ
function buildMessageObject(sender, message) {
  var date = new Date();
  var dateFormat = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  return {'id' : sender, 'msg' : message, 'date' : dateFormat};
}

//  Pseudo random ID (Should use the DB generated ID)
function generateRandomId() {
  return Date.now();
}
