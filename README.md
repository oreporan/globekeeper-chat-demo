# GlobeKeeper Chat service
###### Author: Ore Poran
***
## Instructions
1. Download the zip file (includes source files + rabbitMQ server)
2. run the `./run.sh` script
3. If the browser didn't open automatically, visit http://localhost:3001 and http://localhost:3002
4. to stop the server, run the `./stop.sh` script
