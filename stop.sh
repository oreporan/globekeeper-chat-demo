function clean {
  echo "\nKilling all node instances..."
  killall -9 node 2> /dev/null
  ./rabbitMQ/rabbitmq_server-3.6.1/sbin/rabbitmqctl stop 2> /dev/null
  echo "Bye!"
}

clean
